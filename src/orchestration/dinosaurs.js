import moment from 'moment';
import { isNullOrUndefined } from '../utils/isNullOrUndefined'

export const dinos = (data) => {

    let dinos = [];

    const { dinoFed, dinoList, dinoLocation, dinoRemoved } = data;

    dinos = dinosaursListRemoved(dinoRemoved, dinoList)

    dinos = dinosaursListLocation(dinos, dinoLocation);

    dinos = dinosaursListFeedingTime(dinos,dinoFed)

    dinos = dinosaursListHungry(dinos);

    return dinos;
}

export const dinoRender = ( dino, i, j ) => {
   for(const key in dino){
      if(String(dino[key].location).toLowerCase() === (String.fromCharCode(97 + i)+j)){
        return dino[key]
       }
    }
}

const dinosaursListLocation = ( dinoData, dinoLocation ) => {
    dinoLocation.forEach((dinoLocation) => {
      if(!isNullOrUndefined(dinoData[dinoLocation.dinosaur_id])){
         Object.assign(dinoData[dinoLocation.dinosaur_id],{location:dinoLocation.location, hungry:true})
      }
  }) 
  return dinoData
}


const dinosaursListFeedingTime = ( dinoData, dinoFed ) => {
  dinoFed.forEach(valFed => {
    if (!isNullOrUndefined(dinoData[valFed.dinosaur_id])){
        dinoData[valFed.dinosaur_id].time = valFed.time;
    }
  })
  return dinoData;
}

const dinosaursListHungry = ( dinoData ) => {

    for(var key in dinoData){
          const dateDiff = moment.duration(moment().diff(moment(dinoData[key].time))).asHours();
          dateDiff > dinoData[key].digestion_period_in_hours ? dinoData[key].hungry = true : dinoData[key].hungry = false
    }
    return dinoData;
}   

const dinosaursListRemoved = ( dinoRemoved, dinoData ) => {
    dinoRemoved.forEach(valRemoved => {
      if(!isNullOrUndefined(dinoData[valRemoved.dinosaur_id])){
        delete dinoData[valRemoved.dinosaur_id]
      }
    })
  return dinoData;
}   

