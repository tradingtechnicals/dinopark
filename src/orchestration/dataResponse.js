import { RESPONSE_ADDED, RESPONSE_FED, RESPONSE_LOCATION, RESPONSE_REMOVED, RESPONSE_MAINTENANCE} from '../config';

export const dataSplit = (data) => {

    const dinoList={}, dinoFed = [], dinoLocation=[], dinoRemoved=[], maintenance=[];

    data.forEach(val => {
      if(val.kind === RESPONSE_ADDED && val.herbivore === false){
        dinoList[val.id] = val;
      }
      else if(val.kind === RESPONSE_FED){
        dinoFed.push(val);
      }
      else if(val.kind === RESPONSE_LOCATION){
        dinoLocation.push(val);
      }
      else if(val.kind === RESPONSE_REMOVED){
        dinoRemoved.push(val);
      }
      else if(val.kind === RESPONSE_MAINTENANCE){
        maintenance.push(val);
      }
    });

    const dinoData = {
        dinoList,
        dinoFed,
        dinoLocation,
        dinoRemoved
    }

    return {
        dinoData,
        maintenance
    }
}