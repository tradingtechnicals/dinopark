import moment from 'moment';
import _ from 'lodash'

export const maintainenceOrch = (data) => {
    
    let maintenance = data;

    const maintainGrouped = _.groupBy(maintenance, 'location');

    //get the latest maintainence value for each 
    maintenance = latestMaintainenceRecord(maintainGrouped);

    //assign the maintainence flag to the data to indicate wether we need to do maintainence or not
    maintenance = maintainenceTimeCheck(maintenance);

    //filter out all the areas that do not require maintainence
    const maintainanceNeeded = maintainenceNeeded(maintenance);

    return maintainanceNeeded
}

export const maintenanceRender = (maintainance, i, j) => {
    const foundMaintainanceVal = maintainance.find(val => {
        return String(val.location).toLowerCase() === (String.fromCharCode(97 + i)+j);
      });

    return foundMaintainanceVal;
}

const latestMaintainenceRecord = (maintainGrouped) => {
    const maintainaceList = [];
    Object.keys(maintainGrouped).forEach((key) => {
        maintainaceList.push(maintainGrouped[key][maintainGrouped[key].length -1])
    });

    return maintainaceList;
}   

const maintainenceTimeCheck = (maintainaceList) => {
    maintainaceList.forEach((val, index) => {
        const dateDiff = moment.duration(moment().diff(moment(val.time))).asDays();
        dateDiff > 30 ? Object.assign(maintainaceList[index],{maintainanceNeeded:true}) : Object.assign(maintainaceList[index],{maintainanceNeeded:false}) 
      })

    return maintainaceList;
}

const maintainenceNeeded = (maintainaceList) => {
    const maintainanceNeeded = maintainaceList.filter(val => {
        return val.maintainanceNeeded === true;
      })
    return maintainanceNeeded;
}


