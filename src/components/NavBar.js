import React, { Component } from 'react';
import '../styling/navbar.scss'
import Logo  from '../assets/dinoparks-logo.png'

class NavBar extends Component {

render(){
    return(
        <div className="navbar-fixed">
            <nav className="navBarHeader" >
                <div className="nav-wrapper" >
                    <a href="#!" className="brand-logo">
                        <img src={Logo} alt="" className="navBarLogo"/>
                    </a>
                </div>
            </nav>
        </div>
        )
    }
}

export default NavBar;