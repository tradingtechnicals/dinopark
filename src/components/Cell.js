import React, { Component } from 'react';
import Wrench from '../assets/wrench.png'
import { DINO_MAINTENANCE_HUNGRY, DINO_MAINTENANCE_NOT_HUNGRY, DINO_HUNGRY, DINO_NOT_HUNGRY, MAINTENANCE} from '../config'
import '../styling/cell.scss';


class Cell extends Component {

  cellType(i, j, type ){

    if(type === DINO_HUNGRY)  {
        return (
            <div key={String.fromCharCode(97 + i)+j} className='dinoHungry'/>
        )
    }    
    else if(type === DINO_NOT_HUNGRY)  {
        return (
            <div key={String.fromCharCode(97 + i)+j} className='dinoNotHungry'  />
        )
    }
    else if(type === DINO_MAINTENANCE_HUNGRY)  {
        return (
            <div key={String.fromCharCode(97 + i)+j} className="dinoHungry">
                <img src={Wrench} alt="" className="center imagePng"/>
            </div>
        )
    }
    else if(type === DINO_MAINTENANCE_NOT_HUNGRY)  {
        return (
            <div key={String.fromCharCode(97 + i)+j} className='dinoNotHungry'>
                <img src={Wrench} alt="" className="center imagePng"/>
            </div>
        )
    }
    else if(type === MAINTENANCE){
        return (
            <div key={String.fromCharCode(97 + i)+j} className='blankCell'>
                <img src={Wrench} alt="" className="center imagePng" />
            </div>
        )
    }
    else{
        return (
            <div key={String.fromCharCode(97 + i)+j} className='blankCell'></div>
        )
    }
  }

  render() {

    const { i, j, type } = this.props.setup;

    return this.cellType(i, j, type );
  }
}

export default Cell;
