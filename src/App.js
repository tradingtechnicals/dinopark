import React, { Component } from 'react';
import moment from 'moment';
import NavBar from './components/NavBar';
import axios from 'axios';
import Cell from './components/Cell'
import { dataSplit } from './orchestration/dataResponse';
import { dinos, dinoRender } from './orchestration/dinosaurs'
import { maintainenceOrch, maintenanceRender } from './orchestration/maintenance';
import { sort } from './utils/sort';
import { DINO_MAINTENANCE_HUNGRY, DINO_MAINTENANCE_NOT_HUNGRY, DINO_HUNGRY, DINO_NOT_HUNGRY, MAINTENANCE} from './config'
import './styling/main.scss'

class App extends Component {

  constructor(){
    super();
    this.state = {
      dinoList:[],
      maintainanceNeeded:[],
      error: ''
    };
  }

  processData = (Responsedata) => {


    const data = sort(Responsedata.data, 'newToOld');
    
    const { dinoData, maintenance } = dataSplit(data);

    const dinoList = dinos(dinoData);
    
    const maintainanceNeeded = maintainenceOrch(maintenance);

    this.setState({
      dinoList,
      maintainanceNeeded
    })
  }
 
  componentDidMount(){

      axios.get('https://dinoparks.net/nudls/feed')
      .then(response => this.processData(response))
      .catch(e => {
        this.setState({error:'Error: could not connect to data source'})
      });
  }

  createTable = (dino, maintainance) => {

    let table = [];
    let yAxis = [] 

    const labelsY = []

    for(let a = 1; a <= 16; a++){
      labelsY.push(<div key={`yLabel_${a}`} className="yAxisLabels">{a}</div>)
    }
    
    yAxis.push(<div key="yAxis" className='wrapperYAxis' >{labelsY}</div>)

    for (let i = 0; i <= 25 ; i++) {
      let children = [];
      //Inner loop to create children
        for (let j = 1; j <= 16; j++) {
          //check to see if cell code matches dinasour code e.g. h11 === h11

          const dinoRenderOuput = dinoRender(dino, i, j);
          //check to see if cell code matches maintainance code e.g. h11 === h11
          const maintenance = maintenanceRender(maintainance, i, j);

          if(typeof dinoRenderOuput !== 'undefined') {
            if(typeof maintenance !== 'undefined'){
              const type = dinoRenderOuput.hungry === true ? DINO_MAINTENANCE_HUNGRY : DINO_MAINTENANCE_NOT_HUNGRY;
              children.push(<Cell setup={{j,i,type}}/>)
            }else{
              const type = dinoRenderOuput.hungry === true ? DINO_HUNGRY : DINO_NOT_HUNGRY;
              children.push(<Cell setup={{j,i,type}}/>)
            }
          }
          else if(typeof maintenance !== 'undefined' && maintenance.maintainanceNeeded === true){
            children.push(<Cell setup={{j,i,type:MAINTENANCE}}/>)
          }
          else{
            children.push(<Cell setup={{j,i,type:null}}/>)
          }
        }
      //Create the parent and add the children
      table.push(<div key={`col_${i}`} className='wrapper'>{[children, <div key={`xLabel_${i}`} className="xAxis">{String.fromCharCode(97 + i)}</div>]}</div>)
    }
    table = yAxis.concat(table)
   
    return table
  }

  render() {

    return (
      <div className="App" style={{height:window.innerHeight}} >
        <NavBar/>
        <div className="row mainRowPadding">
          <div className="col s12 m10 l8 offset-m1 offset-l3 dataCol">
            <div className="card-panel z-depth-1 cardPanelCustom">
              <div className="row">
                <h4 className="titleDinoParks">Park Zones</h4>
                 <h5 className="titleDate grey-text lighten-1">{moment().format('DD MMM YYYY')}</h5>
              </div>
              {this.createTable(this.state.dinoList, this.state.maintainanceNeeded)}
            </div>
            <div className="errorMessage">{this.state.error}</div> 
          </div>
        </div>
      </div>
    );
  }
}

export default App;
