import moment from 'moment';

export const sort = (data, type) => {
    if(type === 'newToOld'){
        data.sort(function (a, b) {
            if (moment(a.time) < moment(b.time)) {
                return -1
            } else if (moment(b.time) < moment(a.time)) {    
                return 1
            } else {               
                return 0            
            }
        })
    }
    else{
        data.sort(function (a, b) {
            if (moment(b.time) < moment(a.time)) {
                return -1
            } else if (moment(a.time) < moment(b.time)) {    
                return 1
            } else {               
                return 0            
            }
        })
    }
    return data; 
}