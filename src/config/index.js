export const RESPONSE_ADDED = 'dino_added';
export const RESPONSE_FED = 'dino_fed';
export const RESPONSE_LOCATION = 'dino_location_updated';
export const RESPONSE_REMOVED = 'dino_removed';
export const RESPONSE_MAINTENANCE = 'maintenance_performed';

export const DINO_MAINTENANCE_HUNGRY = 'dino-maintain-hungry';
export const DINO_MAINTENANCE_NOT_HUNGRY = 'dino-maintain-not-hungry';
export const DINO_HUNGRY = 'dino-hungry';
export const DINO_NOT_HUNGRY = 'dino-not-hungry';
export const MAINTENANCE = 'maintenance';


