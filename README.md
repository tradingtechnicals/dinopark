This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Setup
To get started with the project clone this repo: git clone https://gitlab.com/tradingtechnicals/ or dowload the .zip file
once you hav the repo run: npm i 
this will install all the node modules needed.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Approach
The Problem was approached by first downloading the json data object and statically calling it from within the app. 
From there JSON response was grouped by the 'kind' field to see the different responses that the data returns
after this the data was sorted and a hierarchy was created 

### `Dinosaur Heirachy`

1) The data that we need to look at first is the dino_added field as this will supply us with the dinosaurs in the park. Within this field all the herbivorous were removed. 
2) Next the dino_removed data was looked at, as this could immediately get rid of dinosaurs that would never render to the ui.
3) After this the dino_fed data was looked at to update the last time that particular dinosaurs were fed.
4) Lastly the hungry status of the dinosaurs was determined

### `Maintainance Heirachy`

1) All the type "maintenance" was grouped by their location and the latest date from each group was extraced as this would give us a list of the most recent maintainace times for each location e.g. H11. 
2) Next the maintaince last time was compared to the 30 day window, any maintainace falling in the window would return maintainanceNeeded:false outside of the window would return maintainanceNeeded:true.

### `Data merging`
Lastly both data sets were brought together and the zones were rendered accordingly. 

## What you would do differently if you had to do it again
Take more time to read the problem over and visualize it better on a piece of paper.

## What you learned during the project 
Value understanding your task fully before starting, really focusing on trying to improve the performance of data handling

## How you think we can improve this challenge
The challenge is really great, what could possibly make it more comprehensive would be to add real API responses that the server can listen to and/or add more features to the dashboard. All in all as good fun, thanks :). 

